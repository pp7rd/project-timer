import { Observable, PropertyChangeData } from '@nativescript/core';
import { Stopwatch } from "ts-stopwatch";
import { WorkerService } from "../worker.service";

export class HomeViewModel extends Observable
{
    private timerWorker: Worker;
    private workerService: WorkerService;

    private _timer : string;
    private _stopwatch : Stopwatch;

    constructor() 
    {
        super();

        // Initialize default values.
        this._stopwatch = new Stopwatch();
        this._timer = "00:00:00";

        // Worker
        this.workerService = new WorkerService();
        this.timerWorker = this.workerService.initTimerWorker();
        this.timerWorker.onmessage = m => 
        { 
            this.observeTimer();
        }
    }

    // properties

    get timer(): string
    {
        return this._timer;
    }

    set timer(value: string)
    {
        if (value !== this._timer)
        {
            this._timer = value;
            this.notifyPropertyChange('timer', value);
        }
    }

    // methods

    startButtonOnTap() 
    {
        if (!this._stopwatch.isRunning()) 
        {
            this._stopwatch.start();
            this.updateTimer();
            this.timerWorker.postMessage("Timer started");
        }        
    }

    stopButtonOnTap() 
    {
        this._stopwatch.stop();
        this._stopwatch.reset();
    }

    pauseButtonOnTap() 
    {
        this._stopwatch.stop();
    }

    // helper

    // Updating the timer
    private updateTimer() 
    {
        // Get hour, minutes, seconds
        var seconds : number = this._stopwatch.getTime() / 1000;
        var hours: number = Math.round(seconds / 3600);
        var minutes: number = Math.round((seconds % 3600) / 60);
        seconds = Math.round((seconds % 3600) % 60);

        // Formatting
        var hh: string = this.leftPad(hours, 2, '0');
        var mm: string = this.leftPad(minutes, 2, '0');
        var ss: string = this.leftPad(seconds, 2, '0');
        this.timer = hh + ":" + mm + ":" + ss;
    }
    
    private observeTimer() 
    {
        //only when the stopwatch is running the UI will be updated
        if (this._stopwatch.isRunning()) 
        {
            this.updateTimer();
            this.timerWorker.postMessage("Timer continuous");
        }        
    }

    private leftPad(num: number, len: number, ch: string) 
    {
        const str = num.toString();
        len = len - str.length + 1;
        return len > 0 ?
        new Array(len).join(ch) + str : str;
    }
}