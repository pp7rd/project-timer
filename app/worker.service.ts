// add if building with webpack
import * as TimerWorker from "nativescript-worker-loader!./workers/timer.worker";
const workers = [];

export class WorkerService {
    timerWorker: null;
    constructor() {
    }

    initTimerWorker() {
        if (this.timerWorker) {
            return this.timerWorker;
        }

        // add if building with webpack
        this.timerWorker = new TimerWorker();
        workers.push(this.timerWorker);

        return this.timerWorker;
    }
}

if ((<any>module).hot) {
    (<any>module).hot.dispose(() => {
        workers.forEach(w => {
            w.terminate();
        })
    })
}