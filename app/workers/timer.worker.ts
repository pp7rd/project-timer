import "@nativescript/core";
import { Stopwatch } from 'ts-stopwatch';

const context: Worker = self as any;


/*
context.onmessage = msg => {
    //sharedFunction("worker");
    setTimeout(() => {
        console.log("Inside TS worker...");
        console.log(msg);
        (<any>global).postMessage("TS Worker");
    }, 500)
};*/

context.onmessage = msg => {
    //console.log(msg);
    setTimeout(() => {
        //console.log("Timerworker: done waiting for 1s");
        (<any>global).postMessage("done");
    }, 1000)    
}