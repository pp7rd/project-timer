import { Observable } from '@nativescript/core';
import { WorkerService } from "./worker.service";

export class HelloWorldModel extends Observable {
    private timerWorker: Worker;
    private workerService: WorkerService;

    private _counter: number;
    private _message: string;

    constructor() {
        super();

        // Initialize default values.
        this._counter = 42;
        this.updateMessage();

        // Worker
        console.log("Test terminal");
        this.workerService = new WorkerService();
        this.timerWorker = this.workerService.initTimerWorker();
        this.timerWorker.onmessage = m => console.log(m);
        this.timerWorker.postMessage("Timer worker works");
    }

    get message(): string {
        return this._message;
    }

    set message(value: string) {
        if (this._message !== value) {
            this._message = value;
            this.notifyPropertyChange('message', value);
        }
    }

    onTap() {
        this._counter--;
        this.updateMessage();
    }

    private updateMessage() {
        if (this._counter <= 0) {
            this.message =
                'Hoorraaay! You unlocked the NativeScript clicker achievement!';
        } else {
            this.message = `${this._counter} taps left`;
        }
    }
}
